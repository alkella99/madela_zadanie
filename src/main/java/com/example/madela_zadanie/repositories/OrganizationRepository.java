package com.example.madela_zadanie.repositories;

import com.example.madela_zadanie.entities.Organization;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface OrganizationRepository extends PagingAndSortingRepository<Organization, Long> {
    Optional<Organization> findOrganizationByName(String name);
}
