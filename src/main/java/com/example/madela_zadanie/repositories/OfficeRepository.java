package com.example.madela_zadanie.repositories;

import com.example.madela_zadanie.entities.Office;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficeRepository extends JpaRepository<Office, Long> {
}
