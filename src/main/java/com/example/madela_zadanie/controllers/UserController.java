package com.example.madela_zadanie.controllers;

import com.example.madela_zadanie.dto.UserRqDto;
import com.example.madela_zadanie.dto.UserRsDto;
import com.example.madela_zadanie.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public UserRsDto addUser(@RequestBody UserRqDto userRqDto){
        return userService.saveUser(userRqDto);
    }

    @DeleteMapping("/delete")
    public String deleteUser(Long id){
        userService.deleteUser(id);
        return "User was deleted";
    }
}
