package com.example.madela_zadanie.controllers;

import com.example.madela_zadanie.dto.OfficeRqDto;
import com.example.madela_zadanie.dto.OfficeRsDto;
import com.example.madela_zadanie.response.Message;
import com.example.madela_zadanie.services.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/office")
public class OfficeController {
    @Autowired
    private OfficeService officeService;

    @PostMapping("/save")
    public boolean addOffice(@RequestBody OfficeRqDto officeRqDto){
        return officeService.addOffice(officeRqDto);
    }

    @GetMapping("/{id}")
    public OfficeRsDto getOffice(@RequestParam Long id){
        return officeService.getOffice(id);
    }

    @GetMapping("/list")
    public List<OfficeRsDto> listOffices(){
        return officeService.listOffice();
    }

    @PutMapping("/update/{id}")
    public Message updateOffice(@PathVariable Long id, @RequestBody OfficeRqDto officeRqDto){
        officeService.updateOffice(id, officeRqDto);
        return new Message("Success");
    }
}
