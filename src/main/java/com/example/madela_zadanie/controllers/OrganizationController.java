package com.example.madela_zadanie.controllers;

import com.example.madela_zadanie.dto.OrganizationRqDto;
import com.example.madela_zadanie.dto.OrganizationRsDto;
import com.example.madela_zadanie.response.Message;
import com.example.madela_zadanie.services.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/organization")
public class OrganizationController {
    @Autowired
    private OrganizationService organizationService;

    @PostMapping("/save")
    public Message saveOrganization(@RequestBody OrganizationRqDto organizationRqDto){
        organizationService.addOrganization(organizationRqDto);
        return new Message("Success");
    }

    @GetMapping("/{id}")
    public OrganizationRsDto getOrganization(Long id){
        return organizationService.findOrganization(id);
    }

    @GetMapping("/list")
    public List<OrganizationRsDto> listOrganizations(){
        return organizationService.findAllOrganizations();
    }

    @PutMapping("/update/{id}")
    public Message updateOrganization(@PathVariable Long id, @RequestBody OrganizationRqDto organizationRqDto){
        organizationService.updateOrganization(id, organizationRqDto);
        return new Message("Success");
    }
}
