package com.example.madela_zadanie.exceptions;

public class OrganizationAlreadyExistsException extends RuntimeException{
    public OrganizationAlreadyExistsException(String message){
        super(message);
    }
}
