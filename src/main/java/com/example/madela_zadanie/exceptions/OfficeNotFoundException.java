package com.example.madela_zadanie.exceptions;

public class OfficeNotFoundException extends RuntimeException{
    public OfficeNotFoundException(String message){
        super(message);
    }
}
