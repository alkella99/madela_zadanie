package com.example.madela_zadanie.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "organization")
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String fullName;

    private Long inn;

    private Long kpp;

    private String address;

    private Long phone;

    private boolean isActive;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organization")
    @JsonManagedReference
    private List<Office> officeSet;
}
