package com.example.madela_zadanie.dto;

import com.example.madela_zadanie.entities.Office;
import lombok.Data;

import java.util.List;

@Data
public class OrganizationRsDto {
    private String name;

    private String fullName;

    private Long inn;

    private Long kpp;

    private String address;

    private Long phone;

    private List<Office> offices;

    private boolean isActive;
}
