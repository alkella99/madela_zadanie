package com.example.madela_zadanie.dto;

import lombok.Data;

@Data
public class UserRqDto {
    private Long id;

    private String username;

    private String password;

    private String passwordConfirm;

    private String firstName;

    private String secondName;

    private String middleName;

    private String position;

    private Long phone;

    private String docCode;

    private String docNumber;

    private String citizenshipName;

    private String citizenshipCode;

    private boolean isIdentified;
}
