package com.example.madela_zadanie.dto;

import lombok.Data;

@Data
public class OrganizationRqDto {
    private String name;

    private String fullName;

    private Long inn;

    private Long kpp;

    private String address;

    private Long phone;

    private boolean isActive;
}
