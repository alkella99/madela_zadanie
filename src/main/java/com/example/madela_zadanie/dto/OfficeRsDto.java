package com.example.madela_zadanie.dto;

import com.example.madela_zadanie.entities.Organization;
import lombok.Data;

@Data
public class OfficeRsDto {
   private String name;

   private String address;

   private Long phone;

   private boolean isActive;

   private Organization organization;
}
