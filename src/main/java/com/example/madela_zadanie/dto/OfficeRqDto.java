package com.example.madela_zadanie.dto;

import lombok.Data;

@Data
public class OfficeRqDto {
   private String name;

   private String address;

   private Long phone;

   private boolean isActive;

   private Long organizationId;
}
