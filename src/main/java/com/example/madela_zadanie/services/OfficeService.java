package com.example.madela_zadanie.services;

import com.example.madela_zadanie.dto.OfficeRqDto;
import com.example.madela_zadanie.dto.OfficeRsDto;
import com.example.madela_zadanie.entities.Office;

import java.util.List;

public interface OfficeService {
    boolean addOffice(OfficeRqDto officeRqDto);

    OfficeRsDto getOffice(Long id);

    List<OfficeRsDto> listOffice();

    void updateOffice(Long id, OfficeRqDto officeRqDto);
}
