package com.example.madela_zadanie.services;

import com.example.madela_zadanie.dto.OrganizationRqDto;
import com.example.madela_zadanie.dto.OrganizationRsDto;

import java.util.List;

public interface OrganizationService {
    OrganizationRsDto addOrganization(OrganizationRqDto organizationRqDto);

    OrganizationRsDto findOrganization(Long id);

    List<OrganizationRsDto> findAllOrganizations();

    void updateOrganization(Long id, OrganizationRqDto organizationRqDto);
}
