package com.example.madela_zadanie.services;

import com.example.madela_zadanie.dto.UserRqDto;
import com.example.madela_zadanie.dto.UserRsDto;
import com.example.madela_zadanie.entities.User;
import com.example.madela_zadanie.exceptions.PasswordNotMatchException;
import com.example.madela_zadanie.exceptions.UserNotFoundException;
import com.example.madela_zadanie.repositories.RoleRepository;
import com.example.madela_zadanie.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userFromDB = userRepository.findByUsername(username).orElse(new User());
        if(userFromDB.getId() == null){
            throw new UserNotFoundException("User not found by name");
        }
        return userFromDB;
    }

    public UserRsDto saveUser(UserRqDto userRqDto) throws RuntimeException{
        User userFromDB = userRepository.findByUsername(userRqDto.getUsername()).orElse(new User());
        if(userFromDB.getId() != null){
            throw new UserNotFoundException("User already exists");
        }
        if (!userRqDto.getPassword().equals(userRqDto.getPasswordConfirm())){
            throw new PasswordNotMatchException("Passwords not match");
        }

        User user = new User();
        user.setUsername(userRqDto.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(userRqDto.getPassword()));
        userRepository.save(user);
        return modelMapper.map(user, UserRsDto.class);
    }

    public void deleteUser(Long id){
        userRepository.findById(id).orElseThrow(()->new UserNotFoundException("User not found"));
        userRepository.deleteById(id);
    }
}
