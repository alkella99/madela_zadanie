package com.example.madela_zadanie.services.service_impl;

import com.example.madela_zadanie.dto.OfficeRqDto;
import com.example.madela_zadanie.dto.OfficeRsDto;
import com.example.madela_zadanie.entities.Office;
import com.example.madela_zadanie.entities.Organization;
import com.example.madela_zadanie.exceptions.OfficeNotFoundException;
import com.example.madela_zadanie.exceptions.OrganizationNotFoundException;
import com.example.madela_zadanie.repositories.OfficeRepository;
import com.example.madela_zadanie.repositories.OrganizationRepository;
import com.example.madela_zadanie.services.OfficeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OfficeServiceImpl implements OfficeService {
    @Autowired
    private OfficeRepository officeRepository;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private ModelMapper modelMapper;

    public boolean addOffice(OfficeRqDto officeRqDto){
        Organization organization = organizationRepository.findById(officeRqDto.getOrganizationId())
                .orElseThrow(()-> new OrganizationNotFoundException("Organization not found"));
        Office newOffice = new Office();
        newOffice.setName(officeRqDto.getName());
        newOffice.setAddress(officeRqDto.getAddress());
        newOffice.setPhone(officeRqDto.getPhone());
        newOffice.setActive(true);
        newOffice.setOrganization(organization);

        officeRepository.save(newOffice);

        organization.getOfficeSet().add(newOffice);
        organizationRepository.save(organization);
        return true;
    }

    public OfficeRsDto getOffice(Long id){
        Office office = officeRepository.findById(id).orElseThrow(()->new OfficeNotFoundException("Office not found"));
        return modelMapper.map(office, OfficeRsDto.class);
    }

    @Override
    public List<OfficeRsDto> listOffice() {
        List<Office> offices = officeRepository.findAll();
        return offices.stream().map(office -> modelMapper.map(office, OfficeRsDto.class)).collect(Collectors.toList());
    }

    @Override
    public void updateOffice(Long id, OfficeRqDto officeRqDto) {
        Office office = officeRepository.findById(id).orElseThrow(()->
                new OfficeNotFoundException("Office not found"));

//        Organization organization = organizationRepository.findById(office.getOrganization().getId()).orElseThrow(()->
//                new OrganizationNotFoundException("Organization not found"));

        office= modelMapper.map(officeRqDto, Office.class);
//        office.setOrganization(organization);
        officeRepository.save(office);
    }
}
