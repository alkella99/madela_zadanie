package com.example.madela_zadanie.services.service_impl;

import com.example.madela_zadanie.dto.OrganizationRqDto;
import com.example.madela_zadanie.dto.OrganizationRsDto;
import com.example.madela_zadanie.entities.Organization;
import com.example.madela_zadanie.exceptions.OrganizationAlreadyExistsException;
import com.example.madela_zadanie.exceptions.OrganizationNotFoundException;
import com.example.madela_zadanie.repositories.OrganizationRepository;
import com.example.madela_zadanie.services.OrganizationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrganizationServiceImpl implements OrganizationService {
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private ModelMapper modelMapper;

    public OrganizationRsDto addOrganization(OrganizationRqDto organizationRqDto) throws RuntimeException{
        if (!organizationRepository.findOrganizationByName(organizationRqDto.getName()).isEmpty()){
            throw new OrganizationAlreadyExistsException("Organization already exists");
        }

        Organization organization = new Organization();

        organization = modelMapper.map(organizationRqDto, Organization.class);

        return modelMapper.map(organizationRepository.save(organization), OrganizationRsDto.class);
    }

    @Override
    public OrganizationRsDto findOrganization(Long id) {

        Organization organization = organizationRepository.findById(id).orElseThrow(()->
                new OrganizationNotFoundException("Not found"));

        OrganizationRsDto organizationRsDto = modelMapper.map(organization, OrganizationRsDto.class);
        organizationRsDto.setOffices(organization.getOfficeSet());
        return organizationRsDto;
    }

    @Override
    public List<OrganizationRsDto> findAllOrganizations() {
        List<Organization> organizations = (List<Organization>) organizationRepository.findAll();
        return organizations.stream().map(organization -> {
            OrganizationRsDto organizationRsDto = modelMapper.map(organization, OrganizationRsDto.class);
            organizationRsDto.setOffices(organization.getOfficeSet());
        return organizationRsDto;
        }).collect(Collectors.toList());
    }

    @Override
    public void updateOrganization(Long id, OrganizationRqDto organizationRqDto) {
        if(organizationRepository.findById(id).isEmpty()){
            throw new OrganizationNotFoundException("Organization not found");
        }
        organizationRepository.save(modelMapper.map(organizationRqDto, Organization.class));
    }
}
