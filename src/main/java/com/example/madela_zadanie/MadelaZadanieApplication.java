package com.example.madela_zadanie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MadelaZadanieApplication {

    public static void main(String[] args) {
        SpringApplication.run(MadelaZadanieApplication.class, args);
    }

}
